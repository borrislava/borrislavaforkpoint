Google Maps API Task

Create a form with the following input fields – Name, Address,    EMail, Phone and Website.  
 • Each field should be set with a proper input type.  
 • The form must have the first field on focus when the page is loaded.  
 • There should be proper validation for each field (name should not contain special characters, phone should not contain letters and so on).  
 • There should be text under each field in case validation fails. Text needs to be relevant to the error, explaining what is invalid.  
 • The “Address” field must use the Google Maps autocomplete.  
 • In order to submit the form, all fields must be valid.  
 • Upon successful submission, an object with the user information should be saved in the local storage.  
 • If the submitted E-Mail is contained in the local storage, the form should not be submitted and a proper error should be displayed.  
 • Upon map click, the address should be entered in the address input and a pin placed.  • The placed pin must be a custom image. 

DoD (Definition of Done)  
 
Is the validation working properly for each input field?  
After submitting the form, is the address shown on the map without reloading the page?  
Is the user information stored in the local storage after form submission?  
Is the form populated with the address details upon manual map click?  


Calculator Task 

Task instructions: 
-  The calculator should be able to add, subtract, multiply or divide two or more numbers correctly 
- The input field must be validated with a regular expression to accept only numeric and mathematical signs.
- There should be a clear entry (CE) and a clear (C) options provided to the user. 
- The calculator must properly work with decimals.
- The input field should have a maximum limit of how many symbols could be added 
- In case of inappropriate input (divide by zero) – a validation error must be shown. 

DoD (Definition of Done):
- Is the calculator able to divide, multiply, add and subtract properly?
- Is the input validated so that the user enters only correct data?
- Is there a maximum limit of characters for the input field?
- Does the calculator have Clear Entry and Clear options?
- Is the user notified when trying to improperly calculate an expression?

