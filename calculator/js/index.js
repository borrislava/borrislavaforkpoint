$(document).ready(function() {
    var numPattern = /^-?[0-9]{1,8}(\.[0-9]{1,8})?(\+|\-|\/|\*|=)?$/;
    var patternNums = /[0-9]/;
    var patternOperators = /[-+*/=]/;
    var patternPoint = /\./;
    var patternNumWithoutPoint = /-?[0-9]{1,8}/;
    var patternNegative = /\-/;
    var patternDivideByZeroError = /\/0$/;
    var patternDigitsAfterDecimalPoint = /\.[0-9]+$/;
    var patternCheckForTrailingZeroes = /\..+(((0{5,})([1-9]))|0{1,})$/;
    var patternRemoveZeroes = /0{1,}[1-9]?$/;
   
    var currentExpression = "";
    var isTaskFinished = false;
    var islastAddedDigit = true;
   
    $("#number").on("keypress", function(event) {
        var symbol = String.fromCharCode(event.keyCode);
        if (event.keyCode == 13) {
            symbol = "=";
        }
    
        if (patternNums.test(symbol)) {
            addDigit(symbol);
        } else if (patternOperators.test(symbol)) {
            addOperator(symbol);
        } else if (patternPoint.test(symbol)) {
            addPoint(symbol);
        } else {}

        event.preventDefault();
    });
   
    $(".digit").on("click", function(){
        addDigit(this.value);
    });

    function addDigit(digit) {
        if (isTaskFinished) {
            $("#number").val("");
        }

        if (currentExpression != "" && islastAddedDigit == false) {
            $("#number").val("");
        }

        var currentNumber = $("#number").val();
        if (!$.isNumeric(currentNumber) || currentNumber == "0") {
            currentNumber = "";
        }

        var numberUpdated = addCharToExpression(digit, currentNumber, numPattern);
        $("#number").val(numberUpdated);
        islastAddedDigit = true;
        isTaskFinished = false;
    }
   
    function addCharToExpression(charToAdd, strToBeUpdated, pattern) {
        var strUpdated = strToBeUpdated + charToAdd;

        if (pattern.test(strUpdated)) {
            return strUpdated;
        }

        return strToBeUpdated;
    }
   
    $(".operator").on("click", function() {
        addOperator(this.value);
    });
   
    function addOperator(operatorSign) {
        var expressionValue = $("#expression").val();

        if (!islastAddedDigit) {
            currentExpression = expressionValue.substr(0, expressionValue.length - 1) + operatorSign;
        } else {
            var currentNumber = $("#number").val();

            if (!$.isNumeric(currentNumber)) {
                currentNumber = "0";
            }

            if (currentNumber.charAt(currentNumber.length - 1) == ".") {
                currentNumber = currentNumber.substr(0, currentNumber.length - 1);
            }

            var numberOperator = addCharToExpression(operatorSign, currentNumber, numPattern);
            currentExpression = expressionValue + numberOperator;
        }   

        $("#expression").val(currentExpression);
        islastAddedDigit = false;
        showResult(operatorSign);   
    }
   
    $("#point").on("click", function() {
        addPoint(this.value);
    });

    function addPoint(point) {
        if (isTaskFinished) {
            $("#number").val("");
        }

        var currentNumValue = $("#number").val();
        if (!islastAddedDigit || !$.isNumeric(currentNumValue)) {
            $("#number").val("0.");
        } else if (patternPoint.test(currentNumValue)) {
            event.preventDefault();
        } else {
            var numValueUpdated = addCharToExpression(point, currentNumValue, patternNumWithoutPoint);
            $("#number").val(numValueUpdated);
        }

        islastAddedDigit = true;
        isTaskFinished = false;
    }
   
    $("#negative").on("click", function() {
        var number = $("#number").val();
        
        if (!$.isNumeric(number) || number == 0) {
            return;
        }

        this.value = patternNegative.test(number) ? 1 : 0;
        var currentNum = this.value == 0 ? ("-" + number) : number.substr(1);
        $("#number").val(currentNum);
    });
   
    $("#clear").on("click", function() {
        restartInitialValues();
        $("#number").val(null);
    });
   
    $("#clearEntry").on("click", function() {
        $("#number").val(null);
        islastAddedDigit = true;
    });
   
    function showResult(lastOperator) {
        var numberValue = calculateResult(lastOperator);
        $("#number").val(numberValue);
    }
   
    function calculateResult(lastOperator) {
        var expressionWithoutLastSign = currentExpression.substr(0, currentExpression.length - 1);
        
        if (patternDivideByZeroError.test(expressionWithoutLastSign)) {
            restartInitialValues();
            return "Cannot divide by 0.";
        }
        
        var result = eval(expressionWithoutLastSign);
        if (lastOperator == "+" || lastOperator == "-") {
            return removeTrailingZeroes(result);
        } else if (lastOperator == "*" || lastOperator == "/") {
            if (numPattern.test(currentExpression)) {
                return expressionWithoutLastSign;
            }
        } else {
            var digitsAfterDecPoint = getDigitsNumberAfterDecP(result);
            if (digitsAfterDecPoint > 8) {
                result = result.toFixed(8);
                result = removeTrailingZeroes(result);
            }
   
            restartInitialValues();
            return result;
        }
    }
   
    function restartInitialValues() {
        currentExpression = "";
        islastAddedDigit = true;
        isTaskFinished = true;

        $("#negative").val("0");
        $("#expression").val(currentExpression);
    }
   
    function getDigitsNumberAfterDecP(num) {
        num = (num != null) ? num.toString() : "";
        var pointDigitsAfter = num.toString().match(patternDigitsAfterDecimalPoint);
        return (pointDigitsAfter != null) ? (pointDigitsAfter[0].toString().length - 1) : 0;
    }
   
    function removeTrailingZeroes(someValue) {
        var strValue = someValue != null ? someValue.toString() : "";
        if (patternCheckForTrailingZeroes.test(strValue)) {
            return strValue.replace(patternRemoveZeroes, "");
        }

        return someValue;
    }
});