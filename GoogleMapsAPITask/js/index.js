function initMap() {
    var latLng = {lat: 42.6731737, lng: 23.3334113};
    var mapOptions = {
        center: latLng,
        zoom: 15,
        draggable: true
        };

    var map = new google.maps.Map(document.getElementById('map'), mapOptions);
    var input = document.getElementById("address");
    var autocomplete = new google.maps.places.Autocomplete(input);

    autocomplete.bindTo('bounds', map);

    var marker = new google.maps.Marker({
        position: latLng,
        map: map,   
        draggable: false,
        icon: 'images/pinpoint.png'
        });

    autocomplete.addListener('place_changed', function() {
        marker.setVisible(false);
        var place = autocomplete.getPlace();
        var errorLabel = $("label[for='address']");
        if (!place.geometry) {  
            var errorMessage = "No details available for input: '" + place.name + "'";
            errorLabel.text(errorMessage);
            return;
        }

        if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
        } else {
            map.setCenter(place.geometry.location);
            map.setZoom(15);
        }
        
        errorLabel.hide();
        $("#address").val(place.name + " " + place.formatted_address); 
        marker.setPosition(place.geometry.location);
        marker.setVisible(true);
       
    });

    google.maps.event.addListener(map, 'click', function(event) {
        var location = event.latLng;
        setPin(location);
        setAddressInput(location);
    });

    function setPin(location){
        window.setTimeout(function() {marker.setPosition(location);}, 300);
        window.setTimeout(function() {map.panTo(location);}, 300);
    }

    function setAddressInput(location){
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({'location':location}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $("#address").val(results[0].formatted_address);
                    $("#address").trigger("change");
                }
            }
         });
    }
}

$(document).ready(function(){
    var patterns = {
        'name' : /^([a-zA-Z])(([',. \-a-zA-Z])){2,19}$/,
        'phone' : /[+]?([(][0-9]{1,4}[)])?([- \.\/0-9]){5,16}$/,
        'website' : /^(?:http(s)?:\/\/)?[\w.-]+(\.[\w\.-]+)+[\w\-\.~:\/?#[\]@!\$&'\(\)\*\+,;=.]+$/, 
        'email' : /^(.){2,}@(.){2,}\.(.){2,}$/,
        'address' : /^(.){3,}$/
    }

    function validateInput(element){
        var pattern = patterns[$(element).attr("name")];
        var errorLabel = $("label[for='" + element.name + "']");
        var errorMessage = "";

        if (element.validity.valueMissing) {
            errorMessage = "The " + element.name +" is a required field.";
        } else if(!pattern.test(element.value.trim()) || element.validity.typeMismatch){
            errorMessage = "Please enter a valid " + element.name +".";
        } else {
            errorLabel.hide();
            return false;
        }
        errorLabel.text(errorMessage);
        errorLabel.show();
        return true;  
    }

    $(".inputData").on("change", function(){
        validateInput(this);
    });

    $("#sendBtn").on("mousedown",function(){
        var submitTextField = $("#submitResult");
        var errorRedColor = {"color":"red", "border" : "1px solid red"};
        if (!validateForm()){
            submitTextField.css(errorRedColor);
            submitTextField.text("Data could not be sent. Please check if all fields are correct.");
            return;
        }

        var isEmailUsed = checkLocalStorage($("input[name='email']").val());  
        if(isEmailUsed == 0){
            submitTextField.css(errorRedColor);
            submitTextField.text("This email address is already used. Please choose another one.");
            return;
        }else if(isEmailUsed == 1){
            submitTextField.css(errorRedColor);
            submitTextField.text("Your browser does not support local storage. Data could not be saved.");
            return;
        }

        saveUserInfo($("input[name='email']").val().trim());
        submitTextField.css({"color":"green", "border" : "1px solid green"});
        submitTextField.text("Your data was successfully sent!");
        clearFormData();

    });

    function validateForm(){
        var result = true;
        $("input.inputData").each(function(index, element){
            result = !validateInput(element) && result;
        });
        return result;
    }

    function checkLocalStorage(inputChecked){
        var result;
        var localStorage = window.localStorage;
        if (localStorage) {  
            if(localStorage.getItem(inputChecked) != null){
                result = 0;
            }
        } else {
            result = 1;
        }
        return result;
    }

    function saveUserInfo(key){
        inputData = $("form").serializeArray();
        localStorage.setItem(key, JSON.stringify(inputData));
    }

    function clearFormData(){
        $("form").trigger("reset");
    }
});